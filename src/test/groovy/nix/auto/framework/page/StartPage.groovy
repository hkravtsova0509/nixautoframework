package nix.auto.framework.page

import geb.Page

class StartPage extends Page{

    static content = {
        BlogLink (wait:true) { $("a", text: "Blog")}
    }

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def  "User clicks Blog link"(){
        BlogLink.click()
    }
}
